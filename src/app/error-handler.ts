import { ErrorHandler } from "@angular/core";
import { isEqual } from "lodash";

export class BoggleErrorHandler extends ErrorHandler {
    private lastError: unknown;

    handleError(error: unknown): void {
        super.handleError(error);

        if (window.location.hostname === "localhost" && !isEqual(error, this.lastError)) {
            alert(error);
            setTimeout(() => this.lastError = undefined, 5000);
        }
        this.lastError = error;
    }
}
