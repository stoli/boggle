import { Component, OnInit } from "@angular/core";
import { stringify } from "@angular/compiler/src/util";

const buchis = [
  "GNYLUY",
  "BAILBT",
  "SLCLRC",
  "NESOES",
  "NTITEI",
  "ADDAZN",
  "INHSNS",
  "UEKTKE",
  "OFXFAX",
  "SEFEHF",
  "EACEMA",
  "WIRIRU",
  "TUPLUP",
  "ETOITA",
  "RAMISM",
  "OAQBJQ",
];

@Component({
  selector: "app-board",
  templateUrl: "./board.component.html",
  styleUrls: ["./board.component.scss"]
})
export class BoardComponent {
  timer = 180;
  cubes: string[][] = [[], [], [], []];
  CounterEnded = false;

  intervalId?: any;

  startNewGame(): void {
    this.initializeBuchis();
    if (this.intervalId === undefined)
      this.startTimer();
    this.timer = 180;
    this.CounterEnded = false;
  }

  startTimer(): void {
    this.intervalId = setInterval(
      () => {
        this.timer--;
        if (this.timer === 0) {
          this.CounterEnded = true;
          clearInterval(this.intervalId);
          this.intervalId = undefined;
        }
      },
      1000);
  }

  RaiseCurtain(): void {
    this.CounterEnded = false;
  }

  getRandomBuchi(cube: number): string {
    return buchis[cube].charAt(Math.floor(Math.random() * buchis[cube].length));
  }

  initializeBuchis(): void {
    this.cubes = [[], [], [], []];
    for (let cube = 0; cube < 16; cube++) {
      let row: number;
      let col: number;
      do {
        row = Math.floor(Math.random() * 4);
        col = Math.floor(Math.random() * 4);
      } while (this.cubes[row][col] !== undefined);
      this.cubes[row][col] = this.getRandomBuchi(cube);
    }
  }
}
